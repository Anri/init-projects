# Init projects! <!-- omit in toc -->

- [C](#c)
- [C++](#c-1)
- [Java](#java)
- [LaTeX](#latex)
  - [Book](#book)
  - [Document](#document)
  - [Examen](#examen)
  - [Presentation](#presentation)
- [OCaml](#ocaml)
- [Python](#python)
- [Rust](#rust)
- [Typescript](#typescript)

## C

Copy and paste [`c/`](./c/) directory, and you should be good to go!

- Remember to change executable's name and change std's target in
  the [`Makefile`](./c/Makefile).
- **Run `make` to compile the program in release mode.**
- **Run `make debug` to compile the program in debug mode.**
- **Run `make clean` to clean artifacts.**

## C++

Copy and paste [`cpp/`](./cpp/) directory, and you should be good to go!

- Remember to change executable's name and change std's target in
  the [`Makefile`](./cpp/Makefile).
- **Run `make` to compile the program in release mode.**
- **Run `make debug` to compile the program in debug mode.**
- **Run `make clean` to clean artifacts.**

## Java

Copy and paste [`java/`](./java/) directory, and you should be good to go!

- Remember to change executable's name and change std's target in
  the [`Makefile`](./java/Makefile).
- **Run `make` to build and run the program.**
- **Run `make build` to only build the program.**
- **Run `make clean` to clean artifacts.**

## LaTeX

### Book

This is for complex document.

Copy and paste [`latex/book`](./latex/book/) directory,
and you should be good to go!

- **Run `make` to compile the document.**
- **Run `make clean` to clean artifacts.**

> It is by default configured to output `book.pdf`.

### Document

This is for simple document.

Copy and paste [`latex/document`](./latex/document/) directory,
and you should be good to go!

- **Run `make` to compile the document.**
- **Run `make clean` to clean artifacts.**

> It is by default configured to output `document.pdf`.

### Examen

This is for simple test.

Copy and paste [`latex/exam`](./latex/exam/) directory,
and you should be good to go!

- **Run `make` to compile the document.**
- **Run `make clean` to clean artifacts.**

> It is by default configured to output `test.pdf`.

### Presentation

This is for presentations.

Copy and paste [`latex/presentation`](./latex/presentation/) directory,
and you should be good to go!

- **Run `make updatepackage` to download/update dependencie**
  **([projektor](https://git.mylloon.fr/Anri/projektor)).**
- **Run `make` to compile the document.**
- **Run `make clean` to clean artifacts.**

> It is by default configured to build `slides.tex` and output `slides.pdf`.
> There is a more advanced file with more examples named `slides_advanced.tex`.

## OCaml

Copy and paste [`ocaml/`](./ocaml/) directory, and you should be good to go!

- Remember to change values in the [`dune-project`](./ocaml/dune-project).
- **Run `dune exec example` to run the program.**
- **Run `dune runtest` to run tests.**

> Example of custom repository source:
>
> ```dune
> (source
>  (uri https://dev.example.com/project.git))
> ```

> It's recommended to have this one as a "working example", and still create
> new projects via `dune init proj`.

## Python

Copy and paste [`python/`](./python/) directory, and you should be good to go!

- **Run `python main.py` to start the program.**

> It's recommended to use virtual environnement:
>
> - `virtualenv .` → create the virtual env in the current folder
> - `activate` → activate the virtual env
> - Now, it won't affect your other projects

## Rust

Copy and paste [`rust/`](./rust/) directory, and you should be good to go!

- Remember to change values in the [`Cargo.toml`](./rust/Cargo.toml).
- **Run `cargo run` to run the program.**

> It's recommended to have this one as a "working example", and still create
> new projects via `cargo new <directory>`.

## Typescript

Copy and paste [`typescript/`](./typescript/) directory, and you should be good to go!

- Remember to change values in the [`package.json`](./typescript/package.json).
- Then, to install all the dependencies, run `npm install`.
- To check if there is any new update for dependencies, run `npm outdated`, makes
  changes accordingly to run the latest version, you may need to run `npm update`
  to apply the updates after the modification of [`package.json`](./typescript/package.json).
- **Run `npm run main` to start the program.**

---

Contribution? Yes, please!
