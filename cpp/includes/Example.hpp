#pragma once

#include <iostream>

class Example {
  friend std::ostream &operator<<(std::ostream &, const Example &);

public:
  Example();          // constructor
  virtual ~Example(); // destructor

  Example(const Example &);                  // copy constructor
  const Example &operator=(const Example &); // copy assignement
};
