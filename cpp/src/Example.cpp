#include "../includes/Example.hpp"

Example::Example() { std::cout << "Hello, world!\n"; }

Example::~Example() {}

Example::Example(const Example &) {}

const Example &Example::operator=(const Example &src) {
  if (this == &src) {
    return *this;
  }

  return *this;
}
